package cn.uncode.dal.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Database {
	
	/**
	 * 需要切到的数据库
	 * <pre>
	 * 参考DatabaseType：
	 * WRITE：写库 
	 * READ：读库
	 * TRANSACTION：业务处理库，默认为写库，设置该库当前线程将不进行读写分离切换
	 * </pre>
	 * @return
	 */
	DatabaseType value() default DatabaseType.READ;
	
	/**
	 * 支持自定义，必须保证与配置一致
	 * @return
	 */
	String custom() default "";

}
