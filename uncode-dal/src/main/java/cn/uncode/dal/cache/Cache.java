package cn.uncode.dal.cache;


public interface Cache {
    
    int getSize(String tableSpace);

    void putObject(String tableSpace, String key, Object value);
    
    void putObject(String tableSpace, String key, Object value, int seconds);

    Object getObject(String tableSpace, String key);

    Object removeObject(String tableSpace, String key);

    void clear(String tableSpace, String id);

}
