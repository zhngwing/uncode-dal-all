package cn.uncode.dal.springboot.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "uncode.dal.datasource", ignoreInvalidFields = true)
public class ClusterDataSourceConfig{
	
	private List<ClusterDataSourceProperties> clusters;
	
	public List<ClusterDataSourceProperties> getClusters() {
		return clusters;
	}
	public void setClusters(List<ClusterDataSourceProperties> clusters) {
		this.clusters = clusters;
	}
}
