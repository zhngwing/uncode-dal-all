package cn.uncode.dal.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
/**
 * Created by KevinBlandy on 2017/2/28 14:00
 */
@SpringBootApplication
//@EnableDiscoveryClient
@MapperScan(value = "cn.uncode.dal.springboot.mapper",annotationClass = Repository.class)
@ComponentScan({"cn.uncode"})
public class Application extends WebMvcConfigurerAdapter {
	public static void main(String[] agrs){
		SpringApplication.run(Application.class,agrs);
	}
	
/*	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**").excludePathPatterns("/login","/static/**");
	}*/
}
