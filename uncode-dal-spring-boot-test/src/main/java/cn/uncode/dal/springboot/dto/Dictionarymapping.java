package cn.uncode.dal.springboot.dto;

import java.util.Date;
import cn.uncode.dal.core.BaseDTO;
/**
 * 数据库实体类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-04-29
 */
public class Dictionarymapping extends BaseDTO {
	public final static String TABLE_NAME = "dictionarymapping";
	public final static String CHANNELCODE = "channelcode";
	public final static String MAPPINGTYPECODE = "mappingtypecode";
	public final static String INNERVALUE = "innervalue";
	public final static String INNERVALUEREMARK = "innervalueremark";
	public final static String TARGETVALUE = "targetvalue";
	public final static String TARGETVALUEREMARK = "targetvalueremark";
	public final static String STATUS = "status";
	public final static String CREATEBY = "createby";
	public final static String CREATEAT = "createat";
	public final static String MODIFYBY = "modifyby";
	public final static String MODIFYAT = "modifyat";

	/**
	 * 渠道代码，与业务系统的渠道代码相同
	 */
	private String channelcode;
	/**
	 * 字典类型代码，比如城市映射、房源状态映射、订单状态映射等,  取值范围:			
            province-省、 city-城市、region-目的地、bedType-床型、decoration-装修类型、facilities-设施、picture-图片类型、cleaning-打扫类型、indoorPermit-室内活动类型
	 */
	private String mappingtypecode;
	/**
	 * 城家内部值
	 */
	private String innervalue;
	/**
	 * 城家内部值描述
	 */
	private String innervalueremark;
	/**
	 * 渠道方目标值
	 */
	private String targetvalue;
	/**
	 * 渠道方目标值描述
	 */
	private String targetvalueremark;
	/**
	 * 
	 */
	private String status;
	/**
	 * 创建人
	 */
	private String createby;
	/**
	 * 创建时间
	 */
	private Date createat;
	/**
	 * 修改人
	 */
	private String modifyby;
	/**
	 * 修改时间
	 */
	private Date modifyat;

	public String getChannelcode(){
		return this.channelcode;
	}
	public void setChannelcode(String channelcode){
		this.channelcode = channelcode;
	}
	public String getMappingtypecode(){
		return this.mappingtypecode;
	}
	public void setMappingtypecode(String mappingtypecode){
		this.mappingtypecode = mappingtypecode;
	}
	public String getInnervalue(){
		return this.innervalue;
	}
	public void setInnervalue(String innervalue){
		this.innervalue = innervalue;
	}
	public String getInnervalueremark(){
		return this.innervalueremark;
	}
	public void setInnervalueremark(String innervalueremark){
		this.innervalueremark = innervalueremark;
	}
	public String getTargetvalue(){
		return this.targetvalue;
	}
	public void setTargetvalue(String targetvalue){
		this.targetvalue = targetvalue;
	}
	public String getTargetvalueremark(){
		return this.targetvalueremark;
	}
	public void setTargetvalueremark(String targetvalueremark){
		this.targetvalueremark = targetvalueremark;
	}
	public String getStatus(){
		return this.status;
	}
	public void setStatus(String status){
		this.status = status;
	}
	public String getCreateby(){
		return this.createby;
	}
	public void setCreateby(String createby){
		this.createby = createby;
	}
	public Date getCreateat(){
		return this.createat;
	}
	public void setCreateat(Date createat){
		this.createat = createat;
	}
	public String getModifyby(){
		return this.modifyby;
	}
	public void setModifyby(String modifyby){
		this.modifyby = modifyby;
	}
	public Date getModifyat(){
		return this.modifyat;
	}
	public void setModifyat(Date modifyat){
		this.modifyat = modifyat;
	}

}